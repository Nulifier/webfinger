import { test } from "node:test";
import { isEmail } from "./util";
import * as assert from "node:assert";

test("isEmail()", () => {
	assert.ok(isEmail("test@example.com"));
	assert.ok(isEmail("test+add@example.com"));
	assert.ok(!isEmail("www.example.com"));
});
