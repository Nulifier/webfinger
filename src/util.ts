export function isEmail(val: string) {
	// This is a simplified version which is good enough for our uses
	return val.match(/\S+@\S+/) !== null;
}
