import { test } from "node:test";
import { finger } from "./finger";
import * as assert from "node:assert";

function createRequest(resource: string | null, rel: string[] = []) {
	// Create the base URL
	const url = new URL("https://example.com/.well-known/webfinger");

	// Set the resource
	if (resource !== null) {
		url.searchParams.set("resource", resource);
	}

	// Set the relations
	for (let r of rel) {
		url.searchParams.append("rel", r);
	}

	return new Request(url.href);
}

function createEnv() {
	return {};
}

test("A request with no resource is invalid", async () => {
	const req = createRequest(null);
	const env = createEnv();

	const res = await finger(req, env);

	assert.equal(res.status, 400);
	assert.notStrictEqual(res.body, null);
});
