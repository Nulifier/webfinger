import type { Env } from "./definitions";
import { isEmail } from "./util";

const REL_ISSUER = "http://openid.net/specs/connect/1.0/issuer";

export function error(status: number, message: string) {
	return new Response(message, { status });
}

export function normalizeResource(resource: string) {
	// TODO Normalize as per OpenID Connect Discovery protocol
	if (isEmail(resource)) {
		if (resource.startsWith("acct:")) {
			return resource;
		} else {
			return `acct:${resource}`;
		}
	} else {
		return resource;
	}
}

export function getLinks(resource: string, relations: string[]) {
	const links = [];

	if (relations.length === 0 || relations.includes(REL_ISSUER)) {
		// TODO Make this dynamic
		links.push({
			rel: REL_ISSUER,
			href: "https://auth.sunk.io/application/o/tailscale/",
		});
	}

	return links;
}

export async function finger(request: Request, env: Env) {
	const url = new URL(request.url);

	if (true) {
		console.log("Full URL: ", request.url);
		console.log("Headers:");
		request.headers.forEach((val, key) => {
			console.log(`  ${key}: ${val}`);
		});
	}

	// Query must contain exactly one requested resource
	let resource = url.searchParams.get("resource");

	// Query may contain one or more relation parameters
	const relations = url.searchParams.getAll("rel");

	if (resource === null) {
		return error(400, "No resource requested");
	}

	resource = normalizeResource(resource);

	return new Response(
		JSON.stringify({
			subject: resource,
			links: getLinks(resource, relations),
		}),
		{
			headers: {
				"content-type": "application/jrd+json;charset=utf-8",
			},
		}
	);
}
