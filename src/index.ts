import { Router } from "itty-router";
import type { Env } from "./definitions";
import { finger } from "./finger";

const router = Router();

router.get("/.well-known/webfinger/", (request: Request, env: Env) => {
	return finger(request, env);
});

export default {
	fetch: router.handle,
};
